// Fonction pour afficher la mention en fonction de la moyenne
function afficherMention() {
    let moyenne;
  
    // Utiliser une boucle pour demander à l'utilisateur d'entrer une moyenne valide
    while (true) {
      // Demander à l'utilisateur d'entrer une moyenne
      moyenne = parseFloat(prompt("Entrez votre moyenne comprise entre 0 et 20 :"));
  
      // Vérifier si l'entrée est valide
      if (!isNaN(moyenne) && moyenne >= 0 && moyenne <= 20) {
        break; // Sortir de la boucle si la moyenne est valide
      } else {
        // Utiliser alert pour afficher le message d'erreur
        alert("Oups erreur de saisie. Veuillez entrer une moyenne comprise entre 0 et 20.");
      }
    }
    // Afficher la mention en fonction de la moyenne
    if (moyenne >= 18) {
      console.log("Excellent");
    } else if (moyenne >= 16) {
      console.log("Très bien");
    } else if (moyenne >= 14) {
      console.log("Bien");
    } else if (moyenne >= 12) {
      console.log("Assez Bien");
    } else if (moyenne >= 10) {
      console.log("Passable");
    } else if (moyenne >= 8) {
      console.log("Insuffisant");
    } else if (moyenne >= 6) {
      console.log("Faible");
    } else {
      console.log("Médiocre");
    }
  }
  
  // Appeler la fonction pour afficher la mention avec l'entrée de l'utilisateur
  afficherMention();
  