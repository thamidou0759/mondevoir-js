function isPremier(nombre) {
    // Les nombres inférieurs à 2 ne sont pas premiers
    if (nombre < 2) {
      return false;
    }
  
    // Vérifier s'il existe un diviseur entre 2 et la racine carrée du nombre
    for (let i = 2; i <= Math.sqrt(nombre); i++) {
      if (nombre % i === 0) {
        return false; // Le nombre n'est pas premier
      }
    }
  
    return true; // Le nombre est premier
  }
  
  // Tester la fonction avec les exemples donnés
  console.log("0", isPremier(0)); // false
  console.log("1", isPremier(1)); // false
  console.log("2", isPremier(2)); // true
  console.log("3", isPremier(3)); // true
  console.log("11", isPremier(11)); // true
  console.log("12", isPremier(12)); // false
  