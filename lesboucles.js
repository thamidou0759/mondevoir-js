// Fonction pour demander un nombre à l'utilisateur
function demanderNombre() {
    let nombre = parseInt(prompt("Entrez un nombre entre 0 et 10 :"));
  
    // Vérifier si le nombre est valide
    while (isNaN(nombre) || nombre < 0 || nombre > 10) {
      alert("Erreur : Veuillez entrer un nombre valide entre 0 et 10.");
      nombre = parseInt(prompt("Entrez à nouveau un nombre entre 0 et 10 :"));
    }
  
    return nombre;
  }
  
  // Fonction pour afficher les nombres inférieurs ou égaux
  function afficherNombres(nombre) {
    for (let i = nombre; i >= 0; i--) {
      console.log(i);
    }
  }
  
  // Appeler les fonctions
  let nombreUtilisateur = demanderNombre();
  afficherNombres(nombreUtilisateur);
  