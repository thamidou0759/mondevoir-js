// Définition de la classe Rectangle
class Rectangle {
    constructor(largeur, Hauteur) {
      this.largeur = largeur;
      this.hauteur = Hauteur;
    }
  
    // Méthode pour calculer le périmètre du rectangle
    get perimeter() {
      return 2 * (this.largeur + this.hauteur);
    }
  
    // Méthode pour vérifier si le rectangle est valide
    get isValid() {
      return this.largeur > 0 && this.hauteur > 0;
    }
  
    // Méthode pour vérifier si le rectangle est plus grand qu'une autre forme
    isBiggerThan(forme) {
      return this.perimeter > forme.perimeter;
    }
  }
  
  // Définition de la classe Square qui hérite de Rectangle
  class Square extends Rectangle {
    constructor(cote) {
      super(cote, cote); // Appelle le constructeur de la classe parent avec les deux mêmes côtés
    }
  }
  
  // Tester les exemples donnés
  const rectangle1 = new Rectangle(10, 20);
  console.log(rectangle1.perimeter); // 60
  console.log(rectangle1.isValid); // true
  
  const rectangle2 = new Rectangle(-10, 20);
  console.log(rectangle2.isValid); // false
  
  const carre1 = new Square(10);
  console.log(carre1.perimeter); // 40
  console.log(rectangle1.isBiggerThan(carre1)); // true
  